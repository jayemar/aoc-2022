(ns aoc.day07
  (:require [clojure.string :as str]))

(def input (->> "/home/jayemar/projects/aoc-2022/resources/data/input07.txt"
               slurp
               str/split-lines
               ;; (map #(str/split % #"\s+"))
               ))

(def min-size 100000)

(defn not-empty? [l] (not (empty? l)))

(defn not-nil? [i] (not (nil? i)))

(defn cd-cmd
  "Check if command is a cd command, and if so return new directory"
  ([s fs]
   (if-let [cd-dir (cd-cmd s)]
     (cond (and (= cd-dir "/") (get fs cd-dir))
           (do
             ;; (println "cd-cmd cond 1 for cd-dir" cd-dir)
             (assoc fs :wd ["/"]))
           (and (= cd-dir "/"))
           (do
             ;; (println "cd-cmd cond 2 for cd-dir" cd-dir)
             (assoc fs cd-dir {} :wd ["/"]))
           (= cd-dir "..")
           (do
             ;; (println "cd-cmd cond 3 for cd-dir" cd-dir)
             (assoc fs :wd (into [] (drop-last (:wd fs)))))
           :else
           (do
             ;; (println "\nInside else of cd-cmd with cd-dir" cd-dir)
             ;; (println "Inside else of cd-cmd with old wd" (:wd fs))
             ;; (println "Inside else of cd-cmd with new wd" (conj (:wd fs) cd-dir))
             (assoc fs :wd (conj (:wd fs) cd-dir))))))
  ([s]  
   (let [cd (re-matches #"\$ cd (\S+)" s)]
     (if cd (last cd)))))

(defn file-info
  "Check if the line represents a file, and if so return [file-name file-size]"
  ([s fs]
   (if-let [file-entry (file-info s)]
     (let [wd (get fs :wd)
           files (get-in fs wd)]
       (assoc-in fs wd (merge files file-entry)))
     fs))
  ([s]
   (if-let [parts (re-matches #"(\d+)\s+(\w\S+)" s)]
     {(nth parts 2) (parse-long (nth parts 1))})))

(defn dir-info
  "Check if line respresents a directory name, and if so return directory name"
  ([s fs]
   (if-let [dir-name (dir-info s)]
     (let [dir-path (conj (:wd fs) dir-name)]
       (if (not-nil? (get-in fs dir-path))
         fs
         (assoc-in fs (:wd fs) (merge (get-in fs (:wd fs)) {dir-name {}}))))))
  ([s]
   (if-let [parts (re-matches #"^dir (\w+)" s)]
     (second parts))))


(defn resp 
  "Build a filesystem hashmap from input lines"
  [input-lines]
  (loop [fs {}
         ;; lines (take 20 input)
         lines input-lines
         ]
    (if (not-empty? lines)
      (let [l (first lines)]
        (cond (cd-cmd l) (recur (cd-cmd l fs) (rest lines))
              (file-info l) (recur (file-info l fs) (rest lines))
              (dir-info l) (recur (dir-info l fs) (rest lines))
              :else (recur fs (rest lines))))
      fs)))

(def input-resp (resp input))

(defn sum-map
  "Find the disk usage for a given directory map m"
  [m]
  (loop [m m
         s 0]
    (let [v (vals m)
          local-size (reduce + (filter number? v))
          child-dirs (filter not-empty? (filter map? v))
          remaining-dirs (filter not-empty? child-dirs)]
      (if (not remaining-dirs)
        (+ s local-size)
        (+ s local-size (apply + (for [d child-dirs]
                                   (sum-map d))))))))

(println "Part 1:"
         (reduce + (filter #(< % min-size)
                           (map sum-map (filter map?
                                                (tree-seq associative? identity input-resp))))))
;; => 1491614


(def fs-list
  (map sum-map (filter map? (tree-seq associative? identity input-resp))))

(apply max fs-list)
;; => 46090134

(def disk-size 70000000)       ;; 70k
(def req-free-space 30000000)  ;; 30k

(def curr-free-space (- disk-size (apply max fs-list)))
;; => 23909866

(def req-additional-space
  (- req-free-space curr-free-space))
;; => 6090134

(apply min (filter #(>= % req-additional-space) fs-list))
;; => 6400111






(filter #(< % min-size) (map sum-map (filter #(and (map? %) (map? (second %))) (tree-seq associative? identity input-resp))))

(filter #(and (vector? %) (map? (second %))) (tree-seq associative? identity input-resp))

(comment

  (defn dir-sizes
    "directory tree t, solutoin s"
    [t s]

    (println "Entering dir-sizes with s (" (count s) ")" s)
    (let [
          map-keys (filter #(map? (get t %)) (keys t))
          new-ses (zipmap map-keys (map #(sum-map (get t %)) map-keys))
          new-s (merge s new-ses)
          ]
      (if (empty? map-keys)
        (do
          ;; (println "no map keys:" map-keys)
          s)  ;; new-s)
        (do
          ;; (println "we have map keys:" map-keys)
          ;; (map #(dir-sizes (get t %) new-s) map-keys)
          ;; (apply merge (map #(dir-sizes (get t %) new-s) map-keys))
          (merge new-s (apply merge (map #(dir-sizes (get t %) new-s) map-keys)))
          ))

      ;; map-keys
      ;; new-ses

      ))

  ;; use to call sum-map

  (def test-resp
    (dir-sizes (dissoc input-resp :wd) {}))

  (count (dir-sizes (dissoc input-resp :wd) {}))  ;; Should be ~359
  ;; => 135

  (tap> (filter #(<= (val %) min-size) test-resp))
  (reduce + (filter #(<= % min-size) (vals test-resp)))
  ;; => 1037318
  ;; => 1037318

  (map #(val %) test-resp)

  (def input-resp-2 (get input-resp "/"))
  (dir-sizes input-resp-2 {})

  (filter map? (vals input-resp-2))
  (filter #(map? (get input-resp-2 %)) (keys input-resp-2))

  (sum-map input-resp)


  (cd-cmd (nth input 0))
  (cd-cmd (nth input 0) {})
  (cd-cmd (nth input 1))
  (cd-cmd (nth input 1) {})

  (file-info (nth input 2))
  (file-info (nth input 2) {"/" {} :wd ["/"]})
  (file-info (nth input 3))
  (file-info (nth input 3) {})

  (dir-info (nth input 2))
  (dir-info (nth input 2) {})
  (dir-info (nth input 3))
  (dir-info (nth input 3) {"/" {} :wd ["/"]})

  (drop-last [:one :two :three])  ;; returns a list from a vector
  (butlast [:one :two :three])    ;; also return s alist from a vector
  (into [] (drop-last [:one :two :three]))  ;; is there a better way?

  (resp ["$ cd /"
         "$ ls"
         "123 abc"
         "dir foo"
         "$ cd foo"
         "$ ls"
         "456 def"
         "dir bar"
         "$ cd bar"
         "$ ls"
         "135 jabroni"
         "dir baz"
         "246 face"
         ])
  ;; => {"/"
  ;;     {"abc" 123,
  ;;      "foo" {"def" 456, "bar" {"jabroni" 135, "baz" {}, "face" 246}}},
  ;;     :wd ["/" "foo" "bar"]}


  #_(defn dir-sizes
    [directory-tree]
    (loop [t (dissoc directory-tree :wd)
           s 0   ;; size summation for current directory
           v {}] ;; hash map of directory:size for directory tree
      (for [k (keys t)]
        (let [local-size (reduce + (filter number? (vals (get t k))))
              child-dirs (filter not-empty? (filter map? (vals (get t k))))
              remaining-dirs (filter not-empty? child-dirs)]
          (if (not remaining-dirs)
            (+ s local-size)
            (recur child-dirs s v)
            )))))

  (def play
    (dissoc (resp (take 40 input)) :wd))
  ;; => {"/"
  ;;     {"wchdqb" {},
  ;;      "nsphznf" {},
  ;;      "zlpmfh.gpt" 191479,
  ;;      "hmbbjbf" 275929,
  ;;      "phschqg" {},
  ;;      "gwlwp" {},
  ;;      "btm" 282959,
  ;;      "spfwthmd" {},
  ;;      "rhpwvff" 193293,
  ;;      "fmfnpm"
  ;;      {"fgtqvq" {"rjc.ncl" 293783, "wdjrhw" 324635},
  ;;       "fwdvgnqp.fsm" 194704,
  ;;       "fwdwq.tsq" 48823,
  ;;       "mtjngt" 224991,
  ;;       "rdsgpfjb.sfn" 79386,
  ;;       "rvnwwfq" {"btm" 76914},
  ;;       "wrzcjwc"
  ;;       {"fwdwq" {},
  ;;        "fzb.tjs" 2159,
  ;;        "lddhdslh" {},
  ;;        "mjp" {},
  ;;        "vclnlds" 284475},
  ;;       "zlpmfh" {}},
  ;;      "hchp" {},
  ;;      "zlpmfh" {}},
  ;;     :wd ["/" "fmfnpm" "wrzcjwc"]}

  (take 40 input)
  ;; => ("$ cd /"
  ;;     "$ ls"
  ;;     "282959 btm"
  ;;     "dir fmfnpm"
  ;;     "dir gwlwp"
  ;;     "dir hchp"
  ;;     "275929 hmbbjbf"
  ;;     "dir nsphznf"
  ;;     "dir phschqg"
  ;;     "193293 rhpwvff"
  ;;     "dir spfwthmd"
  ;;     "dir wchdqb"
  ;;     "dir zlpmfh"
  ;;     "191479 zlpmfh.gpt"
  ;;     "$ cd fmfnpm"
  ;;     "$ ls"
  ;;     "dir fgtqvq"
  ;;     "194704 fwdvgnqp.fsm"
  ;;     "48823 fwdwq.tsq"
  ;;     "224991 mtjngt"
  ;;     "79386 rdsgpfjb.sfn"
  ;;     "dir rvnwwfq"
  ;;     "dir wrzcjwc"
  ;;     "dir zlpmfh"
  ;;     "$ cd fgtqvq"
  ;;     "$ ls"
  ;;     "293783 rjc.ncl"
  ;;     "324635 wdjrhw"
  ;;     "$ cd .."
  ;;     "$ cd rvnwwfq"
  ;;     "$ ls"
  ;;     "76914 btm"
  ;;     "$ cd .."
  ;;     "$ cd wrzcjwc"
  ;;     "$ ls"
  ;;     "dir fwdwq"
  ;;     "2159 fzb.tjs"
  ;;     "dir lddhdslh"
  ;;     "dir mjp"
  ;;     "284475 vclnlds")

  )
