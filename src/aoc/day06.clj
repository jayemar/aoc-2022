(ns aoc.day06
  (:require [clojure.string :as str]))

(def input (slurp "/home/jayemar/projects/aoc-2022/resources/data/input06.txt"))

(defn diff-pos
  "Find the location in string s where n characters are different"
  [s n]
  (loop [p (partition n 1 s)
         i n]
    (let [f (first p)]
      (if (= (count f) (count (distinct f)))
        i
        (recur (rest p) (inc i))))))

(diff-pos input 4)
;; => 1804

(diff-pos input 14)
;; => 2508
