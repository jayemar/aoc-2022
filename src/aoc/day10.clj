(ns aoc.day10
  (:require [clojure.string :as str]))

(def input (->> "/home/jayemar/projects/aoc-2022/resources/data/input10.txt"
               slurp
               str/split-lines))


(defn convert-line
  [s]
  (cond (str/includes? s "noop") [0]
        (str/includes? s "addx") [0 (-> s (str/split #"\s+") second parse-long)]))

(defn running-total
  [l]
  (reduce (fn [v n] (conj v (+ n (last v)))) [1] l))

;; Part 1
(->> input
     (cons "noop")  ;; add additional noop so that addx operations take 2 cycles
     (mapcat convert-line)
     running-total
     (drop 20)
     (take-nth 40)
     (map * (range 20 221 40))
     (reduce +)
     println)
;; => 14340

;; Part 2
(defn draw-pixel
  "c is the cursor positions, p is the pixel position"
  [c p]
  (if (<= (abs (- p (mod c 40))) 1) \# \.))

(->> input
     (mapcat convert-line)
     running-total
     (map-indexed (fn [idx itm] (draw-pixel idx itm)))
     (partition 40)
     (map #(apply str %))
     (str/join "\n")
     println)
;; => PAPJCBHP
