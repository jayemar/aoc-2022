(ns aoc.day09
  (:require [clojure.string :as str]))

;; [:direction num-steps]
(def input (->> "/home/jayemar/projects/aoc-2022/resources/data/input09.txt"
               slurp
               str/split-lines
               (map #(let [[d n] (str/split % #"\s+")]
                       [(keyword (str/lower-case d))
                        (Integer/parseUnsignedInt n)]))))


(defn board
  "Print the current board of size lxl showing H and T positions
  head vector h of [x y], tail vector t of [x y], board dims [bx by]"
  [[hx hy] [tx ty] [bx by]]
  (let [b (into [] (for [_ (range by)] (into [] (take bx (repeat \.)))))
        b (assoc-in b [ty tx] \T)
        b (assoc-in b [hy hx] \H)
        b (reverse b)]
    (println "\nCurrent Board:  H" [hx hy] " T"[tx ty])
    (doseq [row b]
      (println row))
    b))

(defn move-head
  "Provide new head position based on current position and direction d"
  [[hx hy] d]
  (condp = d
    :u [hx (inc hy)]
    :d [hx (dec hy)]
    :l [(dec hx) hy]
    :r [(inc hx) hy]))

(defn move-tail
  "Give new tail position based on current head and tail positions"
  [[hx hy] [tx ty]]
  (let [x-diff (- tx hx)
        y-diff (- ty hy)]
    (cond (> x-diff  1) [(inc hx) hy]
          (< x-diff -1) [(dec hx) hy]
          (> y-diff  1) [hx (inc hy)]
          (< y-diff -1) [hx (dec hy)]
          :else [tx ty])))

(defn handle-move
  [head-pos tail-pos [d n] moves-vec]
  (if (zero? n)
    moves-vec
    (let [new-head-pos (move-head head-pos d)
          new-tail-pos (move-tail new-head-pos tail-pos)
          new-moves-vec (conj moves-vec [new-head-pos new-tail-pos])]
      (recur new-head-pos new-tail-pos [d (dec n)] new-moves-vec))))

;; v is a vector of moves, m is an individual move (:k n)
(def all-positions
  (reduce (fn [v m]
            (let [[h-pos t-pos] (last v)]
              (handle-move h-pos t-pos m v)))
          [[[0 0] [0 0]]] input))

(println "Part 1:"
         (count (distinct (map second all-positions))))
;; => 6311

#_(defn get-tails
  "hs is a list of head positions, mv is a vector of tail moves"
  ([heads]
   (let [start-pos (first heads)]
     (get-tails heads [start-pos start-pos])))
  ([heads tails]
   (reduce (fn []))
   mv)
  )


(def new-heads (map second all-positions))

;; (let [new-heads (map second all-positions)])  ;; old tails are new-heads

(reduce (fn [tails head]
          (conj tails (move-tail head (last tails))))
        [(first new-heads)]
        new-heads)

(defn heads->tails
  [head-list num-loops]
  (if (zero? num-loops)
    head-list
    (recur
     (reduce (fn [tails head]
               ;; (conj tails (move-tail head (last tails)))
               (let [res (conj tails (move-tail head (last tails)))]
                 ;; (println "new res: " res)
                 res
                ))
             [(first head-list)]
             head-list)
     (dec num-loops))))


(def play
  (heads->tails new-heads 1))

(count (distinct (heads->tails new-heads 8)))
;; => 2455

(count (distinct (heads->tails (map second all-positions) 7)))
;; => 2482  CORRECT ANSWER from Felipe solution

;; => 2714  (7)
;; => 2455  (8)   also TOO LOW, but it's the correct answer for someone.
;;    2456  random-ish guess, but also TOO LOW
;; => 2263  (9)   TOO LOW  also 2264 is TOO LOW
;; => 2099  (10)
;; => 1961  (11)


(def long-test-input
  (map #(let [[d n] (str/split % #"\s+")]
          [(keyword (str/lower-case d))
           (Integer/parseUnsignedInt n)])
       ["R 5"
        "U 8"
        "L 8"
        "D 3"
        "R 17"
        "D 10"
        "L 25"
        "U 20"]))

(def all-positions-test
  (reduce (fn [v m]
            (let [[h-pos t-pos] (last v)]
              (handle-move h-pos t-pos m v)))
          [[[0 0] [0 0]]] long-test-input))

(count (distinct (heads->tails (map second all-positions-test) 8)))
;; => 36

(comment

  (handle-move [0 0] [0 0] (first input) [])

  (mapcat #(handle-move [0 0] [0 0] % []) (take 3 input))

  (board [2 1] [0 3] [6 5])

  (move-head [0 0] :u)
  (move-head [0 0] :d)
  (move-head [0 0] :l)
  (move-head [0 0] :r)

  (move-tail [0 0] [0 0])
  (move-tail [1 0] [0 0])
  (move-tail [1 1] [0 0])
  (move-tail [2 0] [0 0])

  )
