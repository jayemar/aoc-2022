(ns aoc.day13
  (:require [clojure.string :as str]
            [clojure.walk :as w]))

(def in (->> "/home/jayemar/projects/aoc-2022/resources/data/input13.txt"
            slurp
            str/split-lines
            (map clojure.edn/read-string)))

(defn ordered?
  ([[v1 v2]]
   (ordered? v1 v2))
  ([v1 v2]
   (cond (and (number? v1) (number? v2)) (<= v1 v2)
         (and (vector? v1) (vector? v2)) true
         (and (vector? v1) (seq v1)) (<= (first v1) v2)
         (and (vector? v2) (seq v2)) (<= v1 (first v2))
         :else
         (do
           (println "v1: " v1 "\tv2: " v2)
           true))))

(def test-data
  [[1,1,3,1,1]
   [1,1,5,1,1]

   [[1],[2,3,4]]
   [[1],4]

   [9]
   [[8,7,6]]

   [[4,4],4,4]
   [[4,4],4,4,4]

   [7,7,7,7]
   [7,7,7]

   []
   [3]

   [[[]]]
   [[]]

   [1,[2,[3,[4,[5,6,7]]]],8,9]
   [1,[2,[3,[4,[5,6,0]]]],8,9]])

(defn check-order
  [v]
  (map (fn [[v1 v2]] (map ordered? v1 v2)) (partition 2 v)))
