(ns aoc.day03
  (:require [clojure.string :as str]))


(def input-path "/home/jayemar/projects/aoc-2022/resources/data/input03.txt")
(def raw-input (slurp input-path))

(def lines (str/split-lines raw-input))

;; Part 1
;; Verify that all of the values are even and can therefore be split evenly
(every? true? (map even? (map count lines)))
;; => true

(defn split-string
  "Take a string s as input and return a vector of the 2 string halves"
  [s]
  (let [l (count s)
        m (/ l 2)]
    [(subs s 0 m) (subs s m l)]))

(defn char->val
  "Convert a char c represnting an item to the value for that item

  Lowercase item types a through z have priorities 1 through 26.
  Uppercase item types A through Z have priorities 27 through 52."
  [c]
  (let [v (int c)]
    (if (> v 96)
      (- v 96)     ;; for lowercase letters
      (- v 38))))  ;; for uppercase letters

(defn common-element
  "Find the common letter between the strings in the list items"
  [items]
  (first (apply clojure.set/intersection (map set items))))

(def splits (map split-string lines))

(reduce + (map #(char->val (common-element %)) splits))
;; => 7824

;; Part 2
(def groups (partition 3 lines))

(reduce + (map #(char->val (common-element %)) groups))
;; => 2798
