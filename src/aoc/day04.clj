(ns aoc.day04
  (:require [clojure.string :as str]))

(defn- line->ints
  "Split line at , and - and convert values to ints"
  [line]
  (map parse-long (str/split line #",|-")))

;; Split data by line and then on the comma to create a list of 2-tuples
(def inputs (as-> "/home/jayemar/projects/aoc-2022/resources/data/input04.txt" $
              (slurp $)
              (str/split-lines $)
              (map line->ints $)))

(defn part1
  "Compute the subsets of A to B and B to A."
  [[a1 a2 b1 b2]]
  (or (<= a1 b1 b2 a2)
      (<= b1 a1 a2 b2)))

(count (filter true? (map part1 inputs)))
;; => 433

(defn part2
  "Find any sets that have intersections"
  [[a1 a2 b1 b2]]
  (or (<= a1 b1 a2) (<= b1 a1 b2)))

(count (filter true? (map part2 inputs)))
;; => 852
