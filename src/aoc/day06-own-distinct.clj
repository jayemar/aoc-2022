(ns aoc.day06
  (:require [clojure.string :as str]))

(def input (slurp "/home/jayemar/projects/aoc-2022/resources/data/input06.txt"))

(defn not-empty?
  [l]
  (not (empty? l)))

(defn all-different-jmr?
  "Determine if all elements in list l are unique"
  [l]
  (loop [l l]
    (if (< (count l) 2)
      true
      (if (not-empty?
           (filter true?
                   (let [f (first l)]
                     (for [n (rest l)]
                       (= f n)))))
        false
        (recur (rest l))))))

(defn all-different?
  [l]
  (= (count (distinct l)) (count l)))

(defn diff-pos-v1
  "Find the location in string in-str where diff-len characters are different"
  [in-str diff-len]
  (loop [sub (partition diff-len 1 in-str)
         idx diff-len]
    (if (all-different? (first sub))
      idx
      (recur (rest sub) (inc idx)))))

(defn diff-pos
  "Find the location in string in-str where diff-len characters are different"
  [in-str diff-len]
  (loop [sub (partition diff-len 1 in-str)
         idx diff-len]
    (if (= (count (first sub)) (count (distinct (first sub))))
      idx
      (recur (rest sub) (inc idx)))))

(diff-pos input 4)
;; => 1804

(diff-pos input 14)
;; => 2508
