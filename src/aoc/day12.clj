(ns aoc.day12
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; NOTE: The below doesn't work with babashka
;; (def input (slurp (io/resource "data/input12.txt")))
(def in-str (-> "/home/jayemar/projects/aoc-2022/resources/data/input12.txt"
               slurp))

#_(def in-str (str/join "\n" ["Sabqponm"
                            "abcryxxl"
                            "accszExk"
                            "acctuvwj"
                            "abdefghi"]))

(def -cm (str/split-lines in-str));contour map with S and E marks

(def rows (count -cm))
(def cols (count (first -cm)))

(defn- gen-label
  [n]
  (if (= 0 (mod n 10))
    (str (/ (mod n 100) 10))
    " "))

(def row-labels (mapv gen-label (range rows)))
(def col-labels (apply str "  " (map gen-label (range cols))))

(def start (let [s-idx (str/index-of in-str "S")]
             [(mod s-idx (+ cols 1)) (int (/ s-idx cols))]))

(def end (let [e-idx (str/index-of in-str "E")]
             [(mod e-idx (+ cols 1)) (int (/ e-idx cols))]))

(defn print-map
  [map-vec]
  (println
   (str/join "\n" 
             (conj
              (map (fn [r m] (str r " " m)) row-labels map-vec)
              col-labels))))

(defn replace-pos
  "Replace a value in position [x y] with new-val"
  [map-vec [x y] new-val]
  (let [row (get map-vec y)
        new-row (apply str (assoc (vec row) x new-val))]
    (assoc map-vec y new-row)))

(def cm (-> -cm (replace-pos start \a) (replace-pos end \z)))

(defn- valid-loc?
  "Location is valid if it is within the map coordinates"
  [[x y]] (and (>= x 0) (>= y 0) (< x cols) (< y rows)))

(defn- get-neighbors
  "Return a list of the 4 neighbors of location"
  [[x y]]
  (filter valid-loc? [[x (inc y)] [(inc x) y] [x (dec y)] [(dec x) y]]))

(defn height
  "Return height at given location"
  [[x y]]
  (int (get-in cm [y x])))

(defn valid-step?
  "Valid if the next height is not greater than 1 higher than current"
  [loc1 loc2] 
  ;; (<= (- (height loc2) (height loc1)) 1)  ;; for Part 1
  (<= (- (height loc1) (height loc2)) 1)  ;; for Part 2
  )

(defn valid-neighbors
  "Find neighbors that are not too steep to climb"
  [[x y]]
  (filter #(valid-step? [x y] %) (get-neighbors [x y])))

(defn queue [& args]
  "Add elements in args to fifo queue"
  ;; conj - add new item to queue
  ;; peek - see next item in queue
  ;; pop  - return queue without peeked item
  (into clojure.lang.PersistentQueue/EMPTY args))

(defn find-path-len
  "Uses a breadth-first search to find a valid path"
  [start-node stop-node]
  (loop [nodes #{start-node}
         visited-set     #{}
         steps             0]
    (if (visited-set stop-node)
      steps
      (let [new-neighbors (filter
                           #(not (visited-set %))
                           (set (mapcat valid-neighbors nodes)))
            new-visited (set (apply conj visited-set new-neighbors))]
        (recur new-neighbors new-visited (inc steps))))))

;; Part 1
(println
 (find-path-len start end))

;; Part 2
;; an a has a height of 97




(defn find-nearest-a
  "Uses a breadth-first search to find the closest a from E"
  [start-node]
  (loop [nodes #{start-node}
         visited-set     #{}
         steps             1]  ;; why did we star at 1?
    (let [new-neighbors (filter
                         #(not (visited-set %))
                         (set (mapcat valid-neighbors nodes)))
          any-as (filter #(= 97 (height %)) new-neighbors)
          new-visited (set (apply conj visited-set new-neighbors))]
      (if (seq any-as)
        steps
        (recur new-neighbors new-visited (inc steps))))))

(find-nearest-a end)
;; => 430

