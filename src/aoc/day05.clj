(ns aoc.day05
  (:require [clojure.string :as str]))


(def input (-> "/home/jayemar/projects/aoc-2022/resources/data/input05.txt"
               slurp
               str/split-lines))

(defn crate-row?
  "Returns true if the string s is a raw of crates"
  [s]
  (str/includes? s "["))

(defn movement-row?
  "Returns true if the string s is a raw of crates"
  [s]
  (str/starts-with? s "move"))

(def crate-rows (map #(take-nth 4 (rest %)) (filter crate-row? input)))

(def movement-rows (filter movement-row? input))

(defn rows->stacks
  "Take a list of rows and convert to a list of stacks"
  [rows]
  (let [num-stacks (apply max (map count rows))
        num-rows (count rows)
        bottom-up-rows (reverse rows)
        stack-list (for [stack (range num-stacks)
                         row bottom-up-rows]
                     (nth row stack \space))
        shaped-stacks (partition num-rows stack-list)]
    (mapv (fn [stack]
           (filter #(not= \space %) stack))
         shaped-stacks)))


(def stacks (rows->stacks crate-rows))

(def moves (map #(map parse-long (re-seq #"\d+" %)) movement-rows))

;; stacks are 1-indexed, not 0-indexed, so we increment the index
(def stack-map (apply merge
                      (map-indexed (fn [i s] {(inc i) s}) stacks)))

(def final-stacks-part1
  (loop [mvs moves
         stx stack-map]
    (let [[n a b] (first mvs)
          other-moves (rest mvs)
          sa (get stx a)
          sb (get stx b)
          cr (take-last n sa)
          new-a (drop-last n sa)
          new-b (concat sb (reverse cr))
          stx (assoc (assoc stx a new-a) b new-b)]
      (if (empty? other-moves)
        stx
        (recur other-moves stx)))))

(apply str (map #(last (val %)) (sort final-stacks-part1)))
;; => "CWMTGHBDW"

;; Same as part 1, but not calling reverse on the pulled crates
(def final-stacks-part2
  (loop [mvs moves
         stx stack-map]
    (let [[n a b] (first mvs)
          other-moves (rest mvs)
          sa (get stx a)
          sb (get stx b)
          cr (take-last n sa)
          new-a (drop-last n sa)
          new-b (concat sb cr)
          stx (assoc (assoc stx a new-a) b new-b)]
      (if (empty? other-moves)
        stx
        (recur other-moves stx)))))

(apply str (map #(last (val %)) (sort final-stacks-part2)))
;; => "SSCGWJCRB"
