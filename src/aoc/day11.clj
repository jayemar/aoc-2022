(ns aoc.day11
  (:require [clojure.string :as str]))

(defn update-mm
  "Function to update monkey map mm for monkey index mi1"
  [mm mi1]
  (let [mk1 (mi1 mm)
        val ((:op mk1) (peek (:it mk1)))
        mi2 (get (:mv mk1) ((:ts mk1) val))]
    (-> mm
        (update-in [mi1 :it] pop)
        (update-in [mi1 :ct] inc)
        (update-in [mi2 :it] #(conj % val)))))

(defn update-monkey
  "Update monkey with index mi in monkey map mm"
  [mm mi]
  (if (empty? (get-in mm [mi :it]))
    mm
    (recur (update-mm mm mi) mi)))

(defn update-monkeys
  [mm rounds]
  (if (zero? rounds)
    mm
    (recur (reduce #(update-monkey %1 %2) mm (keys mm)) (dec rounds))))

(def lcm (* 11 19 5 3 13 17 7 2))

(def mm
  {:0 {:it [73 77]
       ;; :op #(int (/ (* 5 %) 3))
       :op #(mod (* 5 %) lcm)
       :ts #(= 0 (mod % 11))
       :mv {true :6 false :5}
       :ct 0}
   :1 {:it [57 88 80]
       ;; :op #(int (/ (+ 5 %) 3))
       :op #(mod (+ 5 %) lcm)
       :ts #(= 0 (mod % 19))
       :mv {true :6 false :0}
       :ct 0}
   :2 {:it [61 81 84 69 77 88]
       ;; :op #(int (/ (* % 19) 3))
       :op #(mod (* % 19) lcm)
       :ts #(= 0 (mod % 5))
       :mv {true :3 false :1}
       :ct 0}
   :3 {:it [78 89 71 60 81 84 87 75]
       ;; :op #(int (/ (+ 7 %) 3))
       :op #(mod (+ 7 %) lcm)
       :ts #(= 0 (mod % 3))
       :mv {true :1 false :0}
       :ct 0}
   :4 {:it [60 76 90 63 86 87 89]
       ;; :op #(int (/ (+ 2 %) 3))
       :op #(mod (+ 2 %) lcm)
       :ts #(= 0 (mod % 13))
       :mv {true :2 false :7}
       :ct 0}
   :5 {:it [88]
       ;; :op #(int (/ (+ 1 %) 3))
       :op #(mod (+ 1 %) lcm)
       :ts #(= 0 (mod % 17))
       :mv {true :4 false :7}
       :ct 0}
   :6 {:it [84 98 78 85]
       ;; :op #(int (/ (* % %) 3))
       :op #(mod (* % %) lcm)
       :ts #(= 0 (mod % 7))
       :mv {true :5 false :4}
       :ct 0}
   :7 {:it [98 89 78 73 71]
       ;; :op #(int (/ (+ 4 %) 3))
       :op #(mod (+ 4 %) lcm)
       :ts #(= 0 (mod % 2))
       :mv {true :3 false :2}
       :ct 0}
   })

;; Part 1
;; NOTE: Use first :op lines in mm map
#_(let [mm (update-monkeys mm 20)
      counts (map #(get-in mm [% :ct]) (keys mm))]
  (->> counts
       sort
       reverse
       (take 2)
       (reduce *)))
;; => 56120

;; Part 2
;; NOTE: Use second :op lines in mm map
;; (println "Part 2:")
(let [mm (update-monkeys mm 10000)
      counts (map #(get-in mm [% :ct]) (keys mm))]
  (->> counts
       sort
       reverse
       (take 2)
       (reduce *)))
;; => 24389045529


(comment

  ;;
  ;; TEST DATA
  ;;
  (def test-lcm (* 23 19 13 17))

  (def mm-test
    {:0 {:it [79 98]
         ;; :op #(int (/ (* 19 %) 3))
         :op #(mod (* 19 %) test-lcm)
         :ts #(= 0 (mod % 23))
         :mv {true :2 false :3}
         :ct 0}
     :1 {:it [54 65 75 74]
         ;; :op #(int (/ (+ 6 %) 3))
         :op #(mod (+ 6 %) test-lcm)
         :ts #(= 0 (mod % 19))
         :mv {true :2 false :0}
         :ct 0}
     :2 {:it [79 60 97]
         ;; :op #(int (/ (* % %) 3))
         :op #(mod (* % %) test-lcm)
         :ts #(= 0 (mod % 13))
         :mv {true :1 false :3}
         :ct 0}
     :3 {:it [74]
         ;; :op #(int (/ (+ 3 %) 3))
         :op #(mod (+ 3 %) test-lcm)
         :ts #(= 0 (mod % 17))
         :mv {true :0 false :1}
         :ct 0}})

  ;; Part 1
  ;; NOTE: use the top :op lines
  #_(let [mm (update-monkeys mm-test 20)
          counts (map #(get-in mm [% :ct]) (keys mm))]
      (->> counts
           sort
           reverse
           (take 2)
           (reduce *)))
  ;; => 10605

  ;; Part 2
  ;; NOTE: use the bottom :op lines
  #_(let [mm (update-monkeys mm-test 10000)
          counts (map #(get-in mm [% :ct]) (keys mm))]
      (->> counts
           sort
           reverse
           (take 2)
           (reduce *)))
  ;; => 2713310158


  )
