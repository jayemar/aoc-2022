(ns aoc.day07
  (:require [clojure.string :as str]))

(def input (->> "/home/jayemar/projects/aoc-2022/resources/data/input07.txt"
               slurp
               str/split-lines))

(def min-size 100000)

(defn cd-cmd
  "Check if command is a cd command, and if so return new directory"
  ([s fs]
   (if-let [cd-dir (cd-cmd s)]
     (cond (and (= cd-dir "/") (get fs cd-dir)) (assoc fs :wd ["/"])
           (and (= cd-dir "/")) (assoc fs cd-dir {} :wd ["/"])
           (= cd-dir "..") (assoc fs :wd (into [] (drop-last (:wd fs))))
           :else (assoc fs :wd (conj (:wd fs) cd-dir)))))
  ([s]
   (let [cd (re-matches #"\$ cd (\S+)" s)]
     (if cd (last cd)))))

(defn file-info
  "Check if the line represents a file, and if so return [file-name file-size]"
  ([s fs]
   (if-let [file-entry (file-info s)]
     (let [wd (get fs :wd)
           files (get-in fs wd)]
       (assoc-in fs wd (merge files file-entry)))
     fs))
  ([s]
   (if-let [parts (re-matches #"(\d+)\s+(\w\S+)" s)]
     {(nth parts 2) (parse-long (nth parts 1))})))

(defn dir-info
  "Check if line respresents a directory name, and if so return directory name"
  ([s fs]
   (if-let [dir-name (dir-info s)]
     (let [dir-path (conj (:wd fs) dir-name)]
       (if (some? (get-in fs dir-path))
         fs
         (assoc-in fs (:wd fs) (merge (get-in fs (:wd fs)) {dir-name {}}))))))
  ([s]
   (if-let [parts (re-matches #"^dir (\w+)" s)]
     (second parts))))

(defn input->fs-map
  "Build a filesystem hashmap from input lines"
  [input-lines]
  (loop [fs {}
         lines input-lines]
    (if (seq lines)
      (let [l (first lines)]
        (cond (cd-cmd l) (recur (cd-cmd l fs) (rest lines))
              (file-info l) (recur (file-info l fs) (rest lines))
              (dir-info l) (recur (dir-info l fs) (rest lines))
              :else (recur fs (rest lines))))
      fs)))

(def fs-map (input->fs-map input))

(defn sum-map
  "Find the disk usage for a given directory map m"
  [m]
  (loop [m m
         s 0]
    (let [v (vals m)
          local-size (reduce + (filter number? v))
          child-dirs (filter seq (filter map? v))
          remaining-dirs (filter seq child-dirs)]
      (if (not remaining-dirs)
        (+ s local-size)
        (+ s local-size (apply + (for [d child-dirs]
                                   (sum-map d))))))))

(println "Part 1:"
         (reduce + (filter #(< % min-size)
                           (map sum-map (filter map?
                                                (tree-seq associative? identity fs-map))))))
;; => 1491614

(let [fs-list (map sum-map (filter map? (tree-seq associative? identity fs-map)))
      disk-size 70000000
      req-free-space 30000000
      curr-free-space (- disk-size (apply max fs-list))
      req-additional-space (- req-free-space curr-free-space)]
  (println "Part 2:"
           (apply min (filter #(>= % req-additional-space) fs-list))))
;; => 6400111
