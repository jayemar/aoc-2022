(ns aoc.day02
  (:require [clojure.string :as str]))

(def input2-loc "/home/jayemar/projects/aoc-2022/resources/data/input02.txt")
(def raw-input2 (slurp input2-loc))

(def turns (str/split-lines raw-input2))

(defn turn-score
  "Calculate a score for a turn t

  A turn can match any of the nine possibilities below.

  A for Rock, B for Paper, and C for Scissors (them)
  X for Rock, Y for Paper, and Z for Scissors (me)

  Your total score is the sum of your scores for each round.
  The score for a single round is the score for the shape you selected
  (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the
  outcome of the round
  (0 if you lost, 3 if the round was a draw, and 6 if you won).

  1 for Rock, 2 for Paper, and 3 for Scissors
  0 if you lose, 3 for a draw, and 6 if you win
  "
  [t]
  (cond (= t "A X") (+ 1 3)   ;; rock rock (tie)
        (= t "A Y") (+ 2 6)   ;; rock paper (win)
        (= t "A Z") (+ 3 0)   ;; rock scissors (lose)
        (= t "B X") (+ 1 0)   ;; paper rock (lose)
        (= t "B Y") (+ 2 3)   ;; paper paper (tie)
        (= t "B Z") (+ 3 6)   ;; paper scissors (win)
        (= t "C X") (+ 1 6)   ;; scissors rock (win)
        (= t "C Y") (+ 2 0)   ;; scissors paper (lose)
        (= t "C Z") (+ 3 3)   ;; scissors scissors (tie)
        :else (throw (Exception. :jabroni))))

(defn prepare-turn
  "Create a new turn based the provided elf play p and the desired outcome

  A for Rock, B for Paper, and C for Scissors (them)
  X means you need to lose, Y means you need to draw, and Z means you need to win
  "
  [p]
  (cond (= p "A X") "A Z"   ;; rock lose (scissors)
        (= p "A Y") "A X"   ;; rock tie (rock)
        (= p "A Z") "A Y"   ;; rock win (paper)
        (= p "B X") "B X"   ;; paper lose (rock)
        (= p "B Y") "B Y"   ;; paper tie (paper)
        (= p "B Z") "B Z"   ;; paper win (scissors)
        (= p "C X") "C Y"   ;; scissors lose (paper)
        (= p "C Y") "C Z"   ;; scissors tie (scissors)
        (= p "C Z") "C X"   ;; scissors win (rock)
        :else (throw (Exception. :jabroni))))

;; Part 1
(println "Part 1:"
         (reduce + (map turn-score turns)))
;; => 13565

;; Part 2
(println "Part 2:"
         (reduce + (map turn-score (map prepare-turn turns))))
;; => 12424

