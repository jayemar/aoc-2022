(ns aoc.day08
  (:require [clojure.string :as str])
  (:import [java.lang Exception]))

(def input (slurp "/home/jayemar/projects/aoc-2022/resources/data/input08.txt"))

(defn char->long [l] (parse-long (str l)))


(defn ->tree-grid
  "Output will be a vector of vectors of shape [tree-rows tree-columns]"
  [input-str]
  (let [lines (str/split-lines input-str)
        tree-rows (mapv #(mapv char->long %) lines)
        num-cols (count tree-rows)
        num-rows (count (first tree-rows))
        tree-cols (mapv #(into [] %)
                        (partition num-rows
                                   (for [x (range num-cols)
                                         y (range num-rows)]
                                     (nth (nth tree-rows y) x))))
        ]
    [tree-rows tree-cols]))

(def tree-grid (->tree-grid input))


(defn visible?
  [col row x-pos y-pos]
  (let [tree-height (nth row x-pos)
        l-trees (subvec row 0 x-pos)      ;; left
        r-trees (subvec row (inc x-pos))  ;; right
        u-trees (subvec col 0 y-pos)      ;; up
        d-trees (subvec col (inc y-pos))  ;; down
        vis? (or (empty? l-trees)
                 (> tree-height (apply max l-trees))
                 (empty? r-trees)
                 (> tree-height (apply max r-trees))
                 (empty? u-trees)
                 (> tree-height (apply max u-trees))
                 (empty? d-trees)
                 (> tree-height (apply max d-trees)))]
    vis?))

(defn grid-visibility
  "Count the number of visible trees given an input grid of [rows, cols]"
  [[tree-rows tree-cols]]
  (let [num-rows (count tree-rows)
        num-cols (count tree-cols)
        vis (for [x (range num-cols) y (range num-rows)]
              (visible? (nth tree-cols x) (nth tree-rows y) x y))]
    (count (filter true? vis))))

(println "Part 1:"
 (grid-visibility tree-grid))
;; => 1533


(defn view-distance
  "Find view distance for tree of height h in tree list l"
  [h l]
  (map #(>= % h) l)
  (loop [h h   ;; height
         l l   ;; list of remaining trees
         c 0]  ;; count of visible trees
    (if (empty? l)
      c
      (if (>= (first l) h)
        (inc c)
        (recur h (rest l) (inc c))))))

(defn scenic-score
  "Compute the individual scenic score for a tree with the given x, y value"
  [col row x-pos y-pos]
  (let [tree-height (nth row x-pos)
        l-trees (reverse (subvec row 0 x-pos)) ;; left
        r-trees (subvec row (inc x-pos))       ;; right
        u-trees (reverse (subvec col 0 y-pos)) ;; up
        d-trees (subvec col (inc y-pos))]      ;; down
    (* (view-distance tree-height l-trees)
       (view-distance tree-height r-trees)
       (view-distance tree-height u-trees)
       (view-distance tree-height d-trees))))

(defn max-scenic-score
  "Find the max scenic score for an input grid of [rows, cols]"
  [[tree-rows tree-cols]]
  (let [num-rows (count tree-rows)
        num-cols (count tree-cols)
        scores (for [x (range num-cols) y (range num-rows)]
                 (scenic-score (nth tree-cols x) (nth tree-rows y) x y))]
    (apply max scores)))

(println "Part 2:"
 (max-scenic-score tree-grid))
;; => 345744

