(ns aoc.day01
  (:require [clojure.string :as str]))

(def input1-loc "/home/jayemar/projects/aoc-2022/resources/data/input01.txt")
(def raw-input1 (slurp input1-loc))

(def data1
  (as-> raw-input1 $
    (str/split $ #"\n\n")
    (map #(str/split % #"\n") $)
    (map #(map parse-long %) $)
    (map #(hash-map :snacks % :total (apply + %)) $)
    (zipmap (iterate inc 1) $)))

(first data1)
;; => [249
;;     {:snacks
;;      (2455
;;       4808
;;       1899
;;       1753
;;       1899
;;       5047
;;       3362
;;       5973
;;       1832
;;       3395
;;       2908
;;       2616
;;       5720
;;       2221
;;       1771),
;;      :total 47659}]

;; Part 1
(let [p1 (reduce
          (fn [a b] (if (> (:total (val b)) (:total (val a))) b a))
          data1)]
  (println "Part 1:" (:total (val  p1))))
;; => :total 69693

;; Part 2
(println "Part 2:"
         (apply + (take 3 (reverse (sort (map #(:total (val %)) data1))))))
;; => 200945
